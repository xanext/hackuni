package com.xanext.hackuni.di

import android.app.Application
import com.xanext.hackuni.model.Repository
import com.xanext.hackuni.model.WebService

class App : Application() {


    val webService: WebService by lazy {
        WebService()
    }
    val Repository: Repository by lazy {
        Repository(webService)
    }

}
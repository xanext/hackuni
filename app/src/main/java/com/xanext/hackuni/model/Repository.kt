package com.xanext.hackuni.model

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class Repository(private val webService: WebService) {

    suspend fun getEvents() = withContext(Dispatchers.IO) {
        webService.getEvents()
    }

    suspend fun getReviews() = withContext(Dispatchers.IO) {
        webService.getReview()
    }
}
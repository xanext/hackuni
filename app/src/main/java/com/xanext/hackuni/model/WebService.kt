package com.xanext.hackuni.model

import retrofit2.http.GET
import retrofit2.http.PUT

class WebService {

    @GET()
    suspend fun getEvents() {
    }

    @GET()
    suspend fun getReview() {
    }

    @PUT()
    suspend fun putReview() {
    }

    @PUT()
    suspend fun putTrack() {
    }


}